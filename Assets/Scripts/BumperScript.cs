﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BumperScript : MonoBehaviour {

	public GameObject bumper;
	public GameObject bumperBig;
	public GameObject light;
	public AudioSource ballHit;



	IEnumerator Hit()
	{
		bumper.SetActive (false);
		bumperBig.SetActive (true);
		light.SetActive (true);
		ballHit.Play ();

		yield return new WaitForSeconds (0.1f);

		bumper.SetActive (true);
		bumperBig.SetActive (false);
		light.SetActive (false);

	}

	public void OnCollisionEnter(Collision other)
	{
		if (other.gameObject.name == "Ball") {
			StartCoroutine (Hit ());
		}
	}


}
