﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RespawnBall : MonoBehaviour 
{

	private Vector3 initPos;
	public int lifes;
	public GameObject panel;
	public Text myLife;


	
	void Start () 
	{
		initPos = transform.position;
		lifes = 3;
	}

	void Update()
	{
		myLife.text = "lifes: " + lifes.ToString();
	}

	private void RestartGame()
	{
		transform.position = initPos;
		//lifes -= 1;
		lifes --;
	}

	private void GameOver()
	{
		GUILayout.Label ("GAME OVER");
	}
	
	private void OnCollisionEnter(Collision coll)
	{
		if(coll.gameObject.tag == "spawn")
		{
			if (lifes == 0)
			{
				panel.SetActive (true);
				GameOver();
			}
			else
			{
				RestartGame();
			}
		}
	}
}
