﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DropTarget : MonoBehaviour 
{

	public float dropDistance = 1.0f;
	public int bankID = 0;
	public float resetDeloy = 0.5f;
	public static List<DropTarget> dropTargets = new List<DropTarget>();
	public int targetValue = 550;
	public int bankValue = 5000;

	public bool isDropped = false;
	// Use this for initialization
	void Start () 
	{
		dropTargets.Add(this);
	}
	
	// Update is called once per frame
	void Update () 
	{
		
	}

	void OnCollisionEnter()
	{
		if(!isDropped)
		{
			transform.position +=Vector3.down*dropDistance;
			isDropped = true;
		
			ScoreManager.score += targetValue;

			bool resetBank = true;
			foreach(DropTarget target in dropTargets)
			{
				if(target.bankID == bankID)
				{
					if(!target.isDropped)
					{
						resetBank = false;
					}
				}
			}
			if(resetBank)
			{
				Invoke("ResetBank", resetDeloy);
			}	
		}
	}

	
	void ResetBank()
	{
		foreach(DropTarget target in dropTargets)
		{
			if(target.bankID == bankID)
			{
				target.transform.position +=Vector3.up*dropDistance;
				target.isDropped = false;
			}
		}
	}
}
